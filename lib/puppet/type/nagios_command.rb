Puppet::Type.newtype(:nagios_command) do
  ensurable
  
  newparam(:command_name, :namevar => true) do
    desc 'command name'
  end
  
  newproperty(:command_line) do
    desc 'command line'
  end
  
  newproperty(:target) do
    desc 'target'

    defaultto do
        resource.class.defaultprovider.default_target
    end
  end
end
