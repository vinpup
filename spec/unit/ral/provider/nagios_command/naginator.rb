require File.dirname(__FILE__) + '/../../../../spec_helper'
require 'puppet/provider/nagios_command/naginator'

describe Puppet::Type.type(:nagios_command).provider(:naginator) do
    before do
        @provider_class = Puppet::Type.type(:nagios_command).provider(:naginator)
    end
    
    it 'should have a default target' do
        @provider_class.default_target.should == '/etc/nagios/nagios_command.cfg'
    end

    it 'should add provider instances to target file whose ensure value is present'
    
    it 'should remove provider instances to target file whose ensure value is absent'
    
    it 'should do nothing to provider instances that are and should be present'

    it 'should ignore provider instances that are and should be absent'
    
    it 'should move provider instances from one file to another if the current value is different from the desired value'
    

    it 'should change attrs of provider instances on file to match desired values'

    it 'should add specified attrs of provider instances to file when missing from file'
    
    it 'should remove unspecified attrs of provider instances from file'
    
    it 'should be able to return a list of all provider instances from the default target'
    
    it 'should prefetch attr values from all targets of all given resources'

    it 'should have a toplevel hook that writes all providers to disk'
end
