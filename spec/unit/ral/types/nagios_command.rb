require File.dirname(__FILE__) + '/../../../spec_helper'
require 'puppet/type/nagios_command'

describe Puppet::Type.type(:nagios_command), 'when validating attributes' do
  before do
    @resource_type = Puppet::Type.type(:nagios_command)
  end

  it 'should have a command_name parameter' do
    @resource_type.attrtype(:command_name).should == :param
  end

  it 'should have command_name as the namevar' do
    @resource_type.namevar.should == :command_name
  end
  
  it 'should have a command_line property' do
    @resource_type.attrtype(:command_line).should == :property
  end
  
  it 'should have a target property' do
    @resource_type.attrtype(:target).should == :property
  end
  
  it 'should have an ensure property' do
    @resource_type.attrtype(:ensure).should == :property
  end
end

describe 'when validating ensure attribute values' do
  before do
    @resource_type = Puppet::Type.type(:nagios_command)
  end

  it 'should allow a present value' do
    lambda {@resource_type.create(:name => 'boo', :ensure => :present)}.should_not raise_error
  end
  
  it 'should allow a absent value' do
    lambda {@resource_type.create(:name => 'boo', :ensure => :absent)}.should_not raise_error
  end
  
  it 'should not allow a value that is not absent or present' do
    lambda {@resource_type.create(:name => 'boo', :ensure => :python)}.should raise_error(Puppet::Error)
  end
  
  after do
    @resource_type.clear
  end
end

describe Puppet::Type.type(:nagios_command), "when setting default attribute values" do
    before do
        @resource_type = Puppet::Type.type(:nagios_command)
    end

    it "should default to the provider's default path if one is available" do
        target = '/nagios/locale'
        @resource_type.defaultprovider.stubs(:default_target).returns(target)
    
        resource = @resource_type.create(:name => 'boo')
        resource.should(:target).should == target
    end

    after do
      @resource_type.clear
    end

end
